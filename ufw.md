# ufw
[Uncomplicated Firewall](https://ja.wikipedia.org/wiki/Uncomplicated_Firewall)  


## ufw 自体の有効化・無効化・再起動
| コマンド | 説明・補足 |
| --- | --- |
| `# ufw enable` | 有効化 |
| `# ufw disable` | 無効化 |
| `# ufw reload` | 再読み込み |


## 現在のファイアウォールルール一覧表示
| コマンド | 説明・補足 |
| --- | --- |
| `# ufw status` | 簡易表示 |
| `# ufw status numbered` | ルール番号付きで表示 |
| `# ufw status verbose`  | 詳細表示 |


## ルール追加
サービス名、ポート番号/プロトコル名などで指定ができ、サービス名で追加した場合は ipv6 も追加される。  
また、接続元を指定してルール追加することもできる。
| コマンド例 | 説明・補足 |
| --- | --- |
| `# ufw allow www` | サービス名で許可 |
| `# ufw allow 80/tcp` | ポート番号とプロトコルで許可 |
| `# ufw allow from 192.168.1.0/24 to any port ssh` | 192.168.1.0/24 からの ssh 接続を許可 |
| `# ufw allow proto tcp from 192.168.1.0/24 to any port 22` | 192.168.1.0/24 から 22/tcp での接続を許可 |
| `# ufw deny from 203.0.113.1 to any` | 203.0.113.1 からの全ての接続を拒否 |


## ルール削除
| コマンド例 | 説明・補足 |
| --- | --- |
| `# ufw delete 1` | status numbered の番号で指定 |
| `# ufw delete allow www` | サービス名で指定 |
| `# ufw delete allow 80/tcp` | ポート番号とプロトコルで指定 |


## 参考
- [ufwの基本操作 - Qiita](https://qiita.com/RyoMa_0923/items/681f86196997bea236f0)
- [ufwコマンドの使い方 - Qiita](https://qiita.com/hana_shin/items/a630871dce209cff04f3)
